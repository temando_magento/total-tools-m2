<?php
namespace Temando\Temando\Block\Adminhtml\Shipment;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Helper
     *
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper = null;

    /**
     * Status
     *
     * @var \Temando\Temando\Model\System\Config\Source\Shipment\Status
     */
    protected $_status = null;

    /**
     * Type
     *
     * @var \Temando\Temando\Model\System\Config\Source\Origin\Type
     */
    public $_type = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Temando\Temando\Helper\Data $helper
     * @param array $data
     * @param \Temando\Temando\Model\System\Config\Source\Shipment\Status $status
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        \Temando\Temando\Helper\Data $helper,
        array $data = [],
        \Temando\Temando\Model\System\Config\Source\Shipment\Status $status,
        \Temando\Temando\Model\System\Config\Source\Origin\Type $type
    ) {
        $this->_coreRegistry = $registry;
        $this->_helper = $helper;
        $this->_status = $status;
        $this->_type = $type;
        parent::__construct($context, $data);
    }

    /**
     * Initialize shipment view block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'shipment_id';
        $this->_blockGroup = 'Temando_Temando';
        $this->_controller = 'adminhtml_shipment';

        parent::_construct();

    }

    /**
     * Retrieve text for header element depending on loaded zone
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('temando_shipment')->getId()) {
            return __("View Shipment '%1'", $this->escapeHtml($this->_coreRegistry->registry('temando_shipment')->getId()));
        } else {
            return __('New Shipment');
        }
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('shipment/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

    /**
     * Retrieve current shipment model instance
     *
     * @return \Temando\Temando\Model\Shipment
     */
    public function getShipment()
    {
        return $this->_coreRegistry->registry('temando_shipment');
    }

    /**
     * Get Helper
     *
     * @return Temando/Temando/Helper/Data
     */
    public function getHelper()
    {
        if (!$this->_helper) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $this->_helper = $objectManager->create('Temando\Temando\Helper\Data');
        }
        return $this->_helper;
    }

    /**
     * Return the status of the shipment as text
     *
     * @return string
     */
    public function getShipmentStatusText()
    {
        return $this->_status->getOptionLabel($this->getShipment()->getStatus());
    }


}