<?php
namespace Temando\Temando\Block\Adminhtml\Shipment\Edit;

/**
 * Adminhtml blog post edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_countryFactory;

    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Directory\Model\Config\Source\Country
     * @param \Temando\Temando\Model\Zone\Source\IsActive
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Model\Config\Source\Country $countryFactory,
        \Temando\Temando\Helper\Data $helper,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_countryFactory = $countryFactory;
        $this->_helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('shipment_form');
        $this->setTitle(__('Shipment Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {

        /** @var \Temando\Temando\Model\Shipment $model */
        $model = $this->_coreRegistry->registry('temando_shipment');

        /** @var \Magento\Framework\Data\Form $form */
        $saveUrl = $this->getUrl('temando/shipment/save');

        $form = $this->_formFactory->create(
 //           ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
            ['data' => ['id' => 'edit_form', 'action' => $saveUrl, 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('shipment_');

        $form->setValues($model->getData());

        $destinationFieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Destination'), 'class' => 'fieldset-wide']
        );

        if ($model->getShipmentId()) {
            $destinationFieldset->addField('shipment_id', 'hidden', ['name' => 'shipment_id']);
        }

        $destinationFieldset->addField(
            'destination_contact_name',
            'text',
            ['name' => 'destination_contact_name', 'label' => __('Contact Name'), 'title' => __('Contact Name'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_company_name',
            'text',
            ['name' => 'destination_company_name', 'label' => __('Company Name'), 'title' => __('Company Name'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_street',
            'text',
            ['name' => 'destination_street', 'label' => __('Street'), 'title' => __('Street'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_city',
            'text',
            ['name' => 'destination_city', 'label' => __('City'), 'title' => __('City'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_postcode',
            'text',
            ['name' => 'destination_postcode', 'label' => __('Postcode'), 'title' => __('Postcode'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_region',
            'text',
            ['name' => 'destination_region', 'label' => __('Region'), 'title' => __('Region'), 'required' => true]
        );

        $countryOptions = $this->_countryFactory->toOptionArray();
        $countryField = $destinationFieldset->addField(
            'destination_country',
            'select',
            [
                'name' => 'destination_country',
                'label' => __('Country'),
                'title' => __('Country'),
                // 'onchange' => 'getstate(this)',
                'values' => $countryOptions,
                'value' => $model->getData('destination_country')
            ]
        );

        $destinationFieldset->addField('destination_type', 'checkbox', array(
            'label'     => __('Is Business Address'),
            'name'      => 'destination_type',
            'onclick' => "this.value = this.checked ? '".\Temando\Temando\Model\System\Config\Source\Origin\Type::BUSINESS."' : '".\Temando\Temando\Model\System\Config\Source\Origin\Type::RESIDENTIAL."';"
        ));

        $destinationFieldset->addField('destination_is_signature_required', 'checkbox', array(
            'label'     => __('Is Signature required'),
            'name'      => 'destination_is_signature_required',
            'onclick' => 'this.value = this.checked ? 1 : 0;'
        ));

        $destinationFieldset->addField(
            'destination_phone',
            'text',
            ['name' => 'destination_phone', 'label' => __('Phone'), 'title' => __('Phone'), 'required' => true]
        );

        $destinationFieldset->addField(
            'destination_email',
            'text',
            ['name' => 'destination_email', 'label' => __('Email'), 'title' => __('Email'), 'required' => true]
        );

        $originFieldset = $form->addFieldset(
            'store_fieldset',
            ['legend' => __('Store'), 'class' => 'fieldset-wide']
        );

        $originOptions=$this->_helper->getOriginOptionArray();
        $originField = $originFieldset->addField(
            'origin_id',
            'select',
            [
                'name' => 'origin_id',
                'label' => __('Store'),
                'title' => __('Store'),
                // 'onchange' => 'getstate(this)',
                'values' => $originOptions,
                'value' => $model->getData('origin_id')
            ]
        );

        $form->setValues($model->getData());
        $form->getElement('destination_is_signature_required')->setIsChecked(!empty($model->getData('destination_is_signature_required')));
        $destinationTypeChecked = false;
        if($model->getData('destination_type')==\Temando\Temando\Model\System\Config\Source\Origin\Type::BUSINESS){
            $destinationTypeChecked = true;
        }

        $form->getElement('destination_type')->setIsChecked($destinationTypeChecked);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}