<?php
namespace Temando\Temando\Block\Adminhtml\Origin\Edit;

/**
 * Adminhtml blog post edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $_yesno;

    /**
     * @var \Temando\Temando\Model\Origin\Source\IsActive
     */
    protected $_isActive;

    /**
     * @var \Temando\Temando\Model\System\Config\Source\Origin\Type
     */
    protected $_type;

    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Directory\Model\Config\Source\Country
     * @param \Temando\Temando\Model\Origin\Source\IsActive
     * @param \Temando\Temando\Model\System\Config\Source\Origin\Type
     * @param \Temando\Temando\Helper\Data
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Model\Config\Source\Country $countryFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesno,
        \Temando\Temando\Model\Origin\Source\IsActive $isActive,
        \Temando\Temando\Model\System\Config\Source\Origin\Type $type,
        \Temando\Temando\Helper\Data $helper,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_countryFactory = $countryFactory;
        $this->_yesno = $yesno;
        $this->_isActive = $isActive;
        $this->_type = $type;
        $this->_helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('origin_form');
        $this->setTitle(__('Store Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Temando\Temando\Model\Origin $model */
        $model = $this->_coreRegistry->registry('temando_origin');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $form->setHtmlIdPrefix('origin_');

        $generalFieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getOriginId()) {
            $generalFieldset->addField('origin_id', 'hidden', ['name' => 'origin_id']);
        }

        $generalFieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Name'), 'title' => __('Name'), 'required' => true]
        );

        $generalFieldset->addField(
            'is_active',
            'select',
            [
                'name' => 'is_active',
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => $this->_isActive->toOptionArray(true)
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $generalFieldset->addField(
            'company_name',
            'text',
            ['name' => 'company_name', 'label' => __('Company Name'), 'title' => __('Company Name'), 'required' => true]
        );

        $generalFieldset->addField(
            'erp_id',
            'text',
            [
                'name' => 'erp_id',
                'label' => __('ERP ID'),
                'title' => __('ERP ID'),
                'required' => true,
                'note' => __('Used for synchronising inventory data from third party system.')
            ]
        );

        $generalFieldset->addField(
            'zone_id',
            'select',
            [
                'name' => 'zone_id',
                'label' => __('Zone'),
                'title' => __('Zone'),
                'required' => false,
                'options' => $this->_helper->getZonesOptionArray()
            ]
        );

        $storeIdsField = $generalFieldset->addField(
            'store_ids',
            'multiselect',
            [
                'name' => 'store_ids[]',
                'label' => __('Stores'),
                'title' => __('Stores'),
                'required' => true,
                'values' => $this->_systemStore->toOptionArray(),
                'value' => $model->getStoreIds()
            ]
        );

        $addressFieldset = $form->addFieldset(
            'address_fieldset',
            ['legend' => __('Address'), 'class' => 'fieldset-wide']
        );

        $addressFieldset->addField(
            'street',
            'text',
            ['name' => 'street', 'label' => __('Street'), 'title' => __('Street'), 'required' => true]
        );

        $addressFieldset->addField(
            'city',
            'text',
            ['name' => 'city', 'label' => __('City'), 'title' => __('City'), 'required' => true]
        );

        $addressFieldset->addField(
            'postcode',
            'text',
            ['name' => 'postcode', 'label' => __('Postcode'), 'title' => __('Postcode'), 'required' => true]
        );

        $addressFieldset->addField(
            'region',
            'text',
            ['name' => 'region', 'label' => __('Region'), 'title' => __('Region'), 'required' => true]
        );


        $countryOptions = $this->_countryFactory->toOptionArray();
        $countryField = $addressFieldset->addField(
            'country',
            'select',
            [
                'name' => 'country',
                'label' => __('Country'),
                'title' => __('Country'),
                'values' => $countryOptions,
                'required' => true
            ]
        );

//        $country->setAfterElementHtml("
//            <script type=\"text/javascript\">
//                    require([
//                    'jquery',
//                    'mage/template',
//                    'jquery/ui',
//                    'mage/translate'
//                ],
//                function($, mageTemplate) {
//                   $('#edit_form').on('change', '#origin_country', function(event){
//                        $.ajax({
//                               url : '". $this->getUrl('temando/*/regionlist') . "country/' +  $('#country').val(),
//                                type: 'get',
//                                dataType: 'json',
//                               showLoader:true,
//                               success: function(data){
//                                    $('#origin_region').empty();
//                                    $('#origin_region').append(data.htmlconent);
//                               }
//                            });
//                   })
//                }
//
//            );
//            </script>"
//        );


        $contactFieldset = $form->addFieldset(
            'contact_fieldset',
            ['legend' => __('Contact'), 'class' => 'fieldset-wide']
        );

        $contactFieldset->addField(
            'contact_name',
            'text',
            ['name' => 'contact_name', 'label' => __('Contact Name'), 'title' => __('Contact Name'), 'required' => true]
        );

        $contactFieldset->addField(
            'contact_email',
            'text',
            ['name' => 'contact_email', 'label' => __('Contact Email'), 'title' => __('Contact Email'), 'required' => true, 'class' => 'validate-email']
        );

        $contactFieldset->addField(
            'contact_phone_1',
            'text',
            ['name' => 'contact_phone_1', 'label' => __('Phone 1'), 'title' => __('Phone 2'), 'required' => true, 'class' => 'validate-number']
        );

        $contactFieldset->addField(
            'contact_phone_2',
            'text',
            ['name' => 'contact_phone_2', 'label' => __('Phone 2'), 'title' => __('Phone 2'), 'required' => false, 'class' => 'validate-number']
        );

        $contactFieldset->addField(
            'contact_fax',
            'text',
            ['name' => 'fax', 'label' => __('Fax'), 'title' => __('Fax'), 'required' => false, 'class' => 'validate-number']
        );

        $profileFieldset = $form->addFieldset(
            'profile_fieldset',
            ['legend' => __('Temando Profile'), 'class' => 'fieldset-wide']
        );

        $profileFieldset->addField(
            'account_mode',
            'select',
            [
                'name' => 'account_mode',
                'label' => __('Mode'),
                'title' => __('Mode'),
                'required' => true,
                'values' => [
                    '0' => __('*Use System Configuration'),
                    '1' => __('As Defined')
                ]
            ]
        );

        $profileFieldset->addField(
            'account_sandbox',
            'select',
            [
                'name' => 'account_sandbox',
                'label' => __('Sandbox'),
                'title' => __('Sandbox'),
                'required' => false,
                'values' => $this->_yesno->toOptionArray(),
                'note' => __(
                    'If set to "Yes", the sandbox (testing) service '
                    . 'will be used (usually set to "No" on a live site)'
                ),
            ]
        );

        $profileFieldset->addField(
            'account_clientid',
            'text',
            ['name' => 'account_clientid', 'label' => __('Client ID'), 'title' => __('Client ID'), 'required' => false]
        );

        $profileFieldset->addField(
            'account_username',
            'text',
            ['name' => 'account_username', 'label' => __('Username'), 'title' => __('Username'), 'required' => false]
        );

        $profileFieldset->addField(
            'account_password',
            'password',
            ['name' => 'account_password', 'label' => __('Password'), 'title' => __('Password'), 'required' => false]
        );
        
        $usersFieldset = $form->addFieldset(
            'user_fieldset',
            ['legend' => __('User Permissions'), 'class' => 'fieldset-wide']
        );

        $usersFieldset->addField(
            'user_ids',
            'multiselect',
            [
                'name' => 'user_ids[]',
                'label' => __('Users'),
                'title' => __('Users'),
                'required' => false,
                'style' => 'height:10em',
                'values' => $this->_helper->getUsersOptionArray(),
                'value' => $model->getUserIds()
            ]
        );
        
        /**
        $inventoryFieldset = $form->addFieldset(
            'inventory_fieldset',
            ['legend' => __('Inventory'), 'class' => 'fieldset-wide']
        );

        $inventoryFieldset->addField(
            'inventory',
            'text',
            ['name' => 'inventory', 'label' => __('Inventory'), 'title' => __('Inventory'), 'required' => false]
        );*/


        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}