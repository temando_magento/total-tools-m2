<?php

namespace Temando\Temando\Model\System\Config\Source;

/**
 * System Config Source Labeltype
 *
 * @author Temando Magento Team <marketing@temando.com>
 */
class Labeltype extends \Temando\Temando\Model\System\Config\Source
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
//    const STANDARD = 'Standard';
    const THERMAL  = 'Thermal';

    protected function _setupOptions()
    {
        $this->_options = [
//            self::STANDARD => __('Plain Paper'),
            self::THERMAL  => __('Thermal'),
        ];
    }
}
