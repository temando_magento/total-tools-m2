<?php

namespace Temando\Temando\Model\System\Config\Source;

/**
 * 
 *
 * @author Temando Magento Team <marketing@temando.com>
 */
class Errorprocess extends \Temando\Temando\Model\System\Config\Source
{
    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    const VIEW  = 'view';
    const FLAT  = 'flat';

    protected function _setupOptions()
    {
        $this->_options = [
            self::FLAT  => __('Show flat rate'),
            self::VIEW  => __('Show error message'),
        ];
    }
}
