<?php

namespace Temando\Temando\Model\System\Config\Source\Shipment;

/**
 * System Config Source Shipment Status
 */
class Status extends \Temando\Temando\Model\System\Config\Source
        implements \Magento\Framework\Data\OptionSourceInterface
{

    const PENDING =     '0';
    const PART_BOOKED = '5';
    const BOOKED =      '10';

    protected function _setupOptions()
    {
        $this->_options = array(
            self::PENDING     => __('Pending'),
            self::PART_BOOKED => __('Partially Booked'),
            self::BOOKED      => __('Booked'),
        );
    }
}
