<?php

namespace Temando\Temando\Model\ResourceModel\Origin;

//class Collection extends \Magento\Framework\Api\Search\SearchResult implements \Magento\Framework\Api\Search\SearchResultInterface
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'origin_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        //exit;
        $this->_init('Temando\Temando\Model\Origin', 'Temando\Temando\Model\ResourceModel\Origin');
    }

}