<?php

namespace Temando\Temando\Model\Shipping\Carrier;

use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

/**
 * Temando Carrier Class
 *
 * @author Temando Magento Team <marketing@temando.com>
 */
class Temando extends AbstractCarrier implements CarrierInterface
{

    /**
     * Error Constants
     */
    const ERR_INVALID_COUNTRY = 'Sorry, shipping to selected country is not available.';
    const ERR_INVALID_DEST    = 'Please enter a delivery address to view available shipping methods';
    const ERR_NO_METHODS      = 'No shipping methods available';
    const ERR_INTERNATIONAL   = 'International delivery is not available at this time.';
    const ERR_NO_ORIGIN       = 'Unable to fullfil the order at this time due to missing origin data.';

    const CACHE_LIFETIME = 3600; // 1 Hour
    const CACHE_TAG = 'temando';
    /**
     * Error Map
     *
     * @var array
     */
    protected $_errors_map = array();

    /**
     * Carrier's code
     */
    const CARRIER_CODE = 'temando';
    
    /**
     * @var string
     */
    protected $_code = 'temando';

    /**
     * Carrier's title
     *
     * @var string
     */
    const CARRIER_TITLE = 'Temando';

    /**
     * Rates result
     *
     * @var array|null
     */
    protected $_rates;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Temando\Temando\Model\Origin
     */
    protected $_origin;

    /**
     * @var \Temando\Temando\Model\Resource\Origin
     */
    protected $_originCollection;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Temando\Temando\Model\Api\Request
     */
    protected $_apiRequest;

    /**
     * @var \Temando\Temando\Model\Carrier
     */
    protected $_carrier;

    /**
     * @var \Temando\Temando\Model\Cache\Type
     */
    protected $_cache;

    /**
     * @var \Temando\Temando\Model\ResourceModel\Quote\Collection
     */
    protected $_quoteCollection;

    /**
     * Temando constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Temando\Temando\Model\Carrier $carrier
     * @param \Temando\Temando\Model\Origin $origin
     * @param \Temando\Temando\Model\Resource\Origin\Collection $originCollection
     * @param \Temando\Temando\Model\Api\Request $apiRequest
     * @param \Temando\Temando\Helper\Data $helper
     * @param \Temando\Temando\Model\Cache\Type $cache
     * @param \Temando\Temando\Model\ResourceModel\Quote\Collection $quoteCollection
     * @param \Psr\Log\LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $cart,
        \Temando\Temando\Model\Carrier $carrier,
        \Temando\Temando\Model\Origin $origin,
        \Temando\Temando\Model\Resource\Origin\Collection $originCollection,
        \Temando\Temando\Model\Api\Request $apiRequest,
        \Temando\Temando\Helper\Data $helper,
        \Temando\Temando\Model\Cache\Type $cache,
        \Temando\Temando\Model\ResourceModel\Quote\Collection $quoteCollection,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {

        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
        $this->_origin = $origin;
        $this->_originCollection = $originCollection;
        $this->_apiRequest = $apiRequest;

        $this->_carrier = $carrier;
        $this->_cache = $cache;
        $this->_quoteCollection = $quoteCollection;

        //$this->_cart = $cart;

        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $data
        );
        $this->setErrorsMap();
    }

    /**
     * Returns available shipping methods for current request based on pricing method
     * specified in Temando Settings
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $rateRequest
     * @return Mage_Shipping_Model_Rate_Result|Mage_Shipping_Model_Rate_Result_Error
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $rateRequest)
    {
        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        //check origin/destination country
        if (!$this->_checkAllowedCountries($rateRequest)) {
            $this->_logger->debug('Temando doesn\'t support country ' . $rateRequest->getDestCountryId());
            return;
            // by default no error will show. Remove the above and uncomment
            // the line below to enable an error in the shipping method
            //return $this->_getErrorMethod(self::ERR_INVALID_COUNTRY);
        }

//        $customerAddress = $rateRequest->getCustomerAddress();

        if (!$rateRequest->getDestCountryId()){
            $this->_logger->debug(__('Temando requires a country id, RateRequest doesn\'t have a country id'));
            if (!$this->_scopeConfig->getValue('carriers/temando/showmethod', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
                return;
            }
            return $this->_getErrorMethod(__(self::ERR_INVALID_DEST));
        }

        if (!$rateRequest->getDestPostcode()){
            $this->_logger->debug(__('Temando requires a postcode, RateRequest doesn\'t have a postcode'));
            if (!$this->_scopeConfig->getValue('carriers/temando/showmethod', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
                return;
            }
            return $this->_getErrorMethod(__(self::ERR_INVALID_DEST));
        }

        if(!$rateRequest->getDestCity()){
            $this->_logger->debug(__('Temando requires a city, RateRequest doesn\'t have a city'));
            if (!$this->_scopeConfig->getValue('carriers/temando/showmethod', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
                return;
            }
            return $this->_getErrorMethod(__(self::ERR_INVALID_DEST));
        }

        $salesQuote = $this->_checkoutSession->getQuote();
//        $salesQuoteShippingAddress = $salesQuote->getShippingAddress();
//
//        $salesQuoteShippingAddressCity = $salesQuoteShippingAddress->getCity();
//        $salesQuoteShippingAddressPostcode = $salesQuoteShippingAddress->getPostcode();

        //check PO Box
//        if (!$this->getConfigData('checkout/allow_pobox') &&
//            Mage::helper('temando')->isStreetWithPO($rateRequest->getDestStreet())
//        ) {
//            if (Mage::getStoreConfig('carriers/temando/showmethod')) {
//                return $this->_getErrorMethod(Mage::helper('temando')->__(self::ERR_INVALID_DEST));
//            }
//            return $this->_getCustomErrorMethod($this->getConfigData('checkout/allow_pobox_message'));
//        }

        //get magento sales quote & id


        /* @var $salesQuote \Magento\Quote\Model\Quote */
        //@todo support admin created orders
//        if (!$salesQuote->getId() && Mage::app()->getStore()->isAdmin()) {
//            $salesQuote = Mage::getSingleton('adminhtml/session_quote')->getQuote();
//        }

        //@todo support quotes on product page
//        if ($this->getIsProductPage()) {
//            $salesQuote = Mage::helper('temando')->getDummySalesQuoteFromRequest($rateRequest);
//        }
        $salesQuoteId = $salesQuote->getId();

        //check if eligible for free shipping
        //@todo support free shipping
//        if ($this->isFreeShipping($salesQuote)) {
//            $result->append($this->_getFlatRateMethod('0.00', true));
//            return $result;
//        }

        //get available origin location
        //@todo load relevant origin (according to zones)
//        $origin = Mage::getResourceModel('temando/warehouse_collection')->getOriginByPostcode(
//            $rateRequest->getDestPostcode(),
//            $rateRequest->getDestCountryId(),
//            Mage::app()->getStore()->getId()
//        );
        $this->_origin = $this->_originCollection->getOriginByPostcode($rateRequest->getDestPostcode());
        /* @var $origin \Temando\Temando\Model\Warehouse */

        if (!$this->_origin) {
            //did not find any origins which could serve current request
            if (!$this->_scopeConfig->getValue('carriers/temando/showmethod', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
                return;
            }
            return $this->_getErrorMethod(__(self::ERR_NO_ORIGIN));
        }

        //prepare extras
//        $insurance = Mage::getModel('temando/option_insurance')
//            ->setSetting(Mage::getStoreConfig('temando/insurance/status'));
//        $carbon = Mage::getModel('temando/option_carbonoffset')
//            ->setSetting(Mage::getStoreConfig('temando/carbon/status'));
//        $footprints = Mage::getModel('temando/option_footprints')
//            ->setSetting(Mage::getStoreConfig('temando/footprints/status'));
//
//        if ($this->getIsProductPage() || $this->getIsCartPage()) {
//            if (!in_array(
//                $insurance->getForcedValue(),
//                array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO)
//            )
//            ) {
//                $insurance->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
//            }
//
//            if (!in_array(
//                $carbon->getForcedValue(),
//                array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO)
//            )
//            ) {
//                $carbon->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
//            }
//
//            if (!in_array(
//                $footprints->getForcedValue(),
//                array(Temando_Temando_Model_Option_Boolean::YES, Temando_Temando_Model_Option_Boolean::NO)
//            )
//            ) {
//                $footprints->setForcedValue(Temando_Temando_Model_Option_Boolean::NO);
//            }
//        }
//        /* @var Temando_Temando_Model_Options $options */
//        $options = Mage::getModel('temando/options')->addItem($insurance)->addItem($carbon)->addItem($footprints);
//
//        //save current extras
//        if (is_null(Mage::registry('temando_current_options'))) {
//            Mage::register('temando_current_options', $options);
//        }

        //init rule engine, preload applicable rules
        //@todo rule engine
//        $ruleEngine = Mage::getModel('temando/hybrid')->loadRules($salesQuote);
//        /* @var $ruleEngine Temando_Temando_Model_Hybrid */
//        if (!$ruleEngine->hasValidRules()) {
//            //no valid rules => no available shipping methods
//            if (!Mage::getStoreConfig('carriers/temando/showmethod')) {
//                return;
//            }
//            return $this->_getErrorMethod(Mage::helper('temando')->__(self::ERR_NO_METHODS));
//        }
//        if ($ruleEngine->hasRestrictive()) {
//            //shipping to current destination is restricted, show custom message from rule engine
//            if (Mage::getStoreConfig('carriers/temando/showmethod')) {
//                return $this->_getErrorMethod(Mage::helper('temando')->__(self::ERR_NO_METHODS));
//            }
//            return $this->_getCustomErrorMethod($ruleEngine->getRestrictiveNote());
//        }

        //get available quotes from the API for current request
        $quotes = array();
        //if ($ruleEngine->hasDynamic()) {
        if(true){//@todo rule-engine lookup
            //check if current request same as previous & dynamic rules configured
            $lastRequestString = $this->_checkoutSession->getTemandoRequestString();
            $this->_logger->debug(get_class($this) . ' session request string ('.$lastRequestString.')');
            //@todo check a force refresh variable?
            if ($lastRequestString == $this->_createRequestString($rateRequest, $salesQuoteId)){
                //request is the same as previous and delivery options not changed - load existing quotes from DB
                $this->_logger->debug(get_class($this) . ' fetching quote from DB');
                $collection = $this->_quoteCollection
                    ->addFieldToFilter('magento_quote_id', $salesQuoteId)
                    ->load();
//                if ($this->getDeliverBy() !== false) {
//                    $collection
//                        ->addFieldToFilter(
//                            'eta_to',
//                            array(
//                                'lteq' => Mage::helper('temando')
//                                    ->getMaxEtaForDeliveryByDate($this->getDeliverBy())
//                            )
//                        );
//                }
                $quotes = $collection->getItems();
            } else {
                try {
                    //@todo figure out if we need ready_date, deliveryby, deliveryoptions
                    /* @var $apiRequest \Temando\Temando\Model\Api\Request */

                    $boxesRequired = $this->_helper->calculateBoxes($rateRequest->getAllItems());
                    
                    $this->_apiRequest
                        ->setConnectionParams($this->_origin->getTemandoProfile())
                        ->setMagentoQuoteId($salesQuoteId)
                        ->setDestination(
                            $rateRequest->getDestCountryId(),
                            $rateRequest->getDestPostcode(),
                            $rateRequest->getDestCity(),
                            $rateRequest->getDestStreet(),
                            $rateRequest->getDestType()
                        )
                        ->setOrigin($this->_origin->getName(), $this->_origin->getCountry())
                        //->setItems($rateRequest->getAllItems())
                        ->setItems($boxesRequired)
                        ->setReady($this->_helper->getReadyDate())
                        //->setDeliverBy($this->getDeliverBy())
                        ->setDeliveryOptions($rateRequest->getDeliveryOptions())
                        ->setAllowedCarriers($this->getAllowedMethods());

                    $requestStr = print_r($this->_apiRequest->toRequestArray(), 1);

                    $collectionStr = $this->_cacheLoadShippingOptions($requestStr);

                    if($collectionStr) {
                        $collection = unserialize($collectionStr);
                    } else {

                        $collection = $this->_apiRequest->getQuotes();

                        if(is_soap_fault($collection)){
                            $this->_logger->debug('Found SOAP Fault');
                            $errorMessage = $collection->getMessage().' ('.$collection->getCode().')';
                            $this->_logger->debug($errorMessage);
                            return;
                        }
                        $this->_cacheSaveShippingOptions($requestStr, serialize($collection));
                    }

                    if($collection){
//                        if ($this->getDeliverBy() !== false) {
//                            $collection
//                                ->addFieldToFilter(
//                                    'eta_to',
//                                    array(
//                                        'lteq' => Mage::helper('temando')
//                                            ->getMaxEtaForDeliveryByDate($this->getDeliverBy())
//                                    )
//                                );
//                        }

                        $quotes = $collection->getItems();
                        $requestString = $this->_createRequestString($rateRequest, $salesQuoteId);
                        $this->_logger->debug(get_class($this) . ' saving request string ('.$requestString.')');
                        $this->_checkoutSession
                            ->setTemandoRequestString(
                                $requestString
                            );

                    } else {
                        $this->_logger->debug(get_class($this) . ' FALSE collection returned from $this->_apiRequest->getQuotes()');
                    }

                } catch (Exception $e) {
                    $this->_logger->debug(get_class($this).' ' .$e->getMessage());
                    return;
                    //@todo catch the errors neatly
//                    switch($this->_scopeConfig->getValue('temando/options/error_process', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
//                        case Temando_Temando_Model_System_Config_Source_Errorprocess::VIEW:
//                            if (!Mage::getStoreConfig('carriers/temando/showmethod')) {
//                                return;
//                            }
//                            return $this->_getErrorMethod($e->getMessage());
//                            break;
//                        case Temando_Temando_Model_System_Config_Source_Errorprocess::CUSTOM:
//                            if (!Mage::getStoreConfig('carriers/temando/showmethod')) {
//                                return;
//                            }
//                            return $this->_getErrorMethod(
//                                Mage::helper('temando')->getConfigData('options/error_process_message')
//                            );
//                            break;
//                        case Temando_Temando_Model_System_Config_Source_Errorprocess::FLAT:
//                            $result->append($this->_getFlatRateMethod());
//                            return $result;
//                            break;
//                    }
                }
            }





        }
        //$this->registerItems($salesQuote);
        //get shipping methods
        //$shippingMethods = $ruleEngine->getShippingMethods($options, $quotes);
        unset($shippingMethods);
        $shippingMethods = $quotes;
        //set hasDynamic for checkout shipping methods template
//        if ($ruleEngine->hasDynamic() && !empty($quotes)) {
//            Mage::register('temando_has_dynamic_shipping_methods', true, true);
//        }
        if (empty($shippingMethods)) {//@todo catch error properly
              //if (!Mage::getStoreConfig('carriers/temando/showmethod')) {
//                return;
            //}
//            return $this->_getErrorMethod(Mage::helper('temando')->__(self::ERR_NO_METHODS));
        } else {
            $c = 0;
            foreach ($shippingMethods as $shippingMethod) {
                $carrier = $this->_helper->getCarrierByTemandoId($shippingMethod->getCarrierId());
                if(($shippingMethod->getDeliveryMethod() == '') || ($shippingMethod->getTotalPrice() == 0)){
                    continue;
                }
                $method = $this->_rateMethodFactory->create();

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($carrier->getCompanyName());

                $method->setMethod($this->_code."_".$shippingMethod->getQuoteId());
                $method->setMethodTitle($shippingMethod->getDeliveryMethod());

                $method->setPrice($shippingMethod->getTotalPrice());
                $method->setCost($shippingMethod->getBasePrice());
                $result->append($method);
            }
        }
        //Mage::register('temando_has_new_shipping_methods', true, true);

        return $result;
    }


    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        $allowed = explode(',', $this->getConfigData('allowed_methods'));
        $arr = [];
        foreach ($allowed as $k) {
            $arr[$k] = $this->getCode('method', $k);
        }
        return $arr;
    }

    /**
     * Returns Temando carrier code
     *
     * @return string
     */
    static public function getCode()
    {
        return self::CARRIER_CODE;
    }

    /**
     * Checks if the to address is within allowed countries
     *
     * @return boolean
     */
    protected function _checkAllowedCountries(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        if($request->getDestCountryId() == 'AU') {//@todo make this lookup from system config properly
            return true;
        }
        return false;
        return array_key_exists($request->getDestCountryId(), $this->_helper->getAllowedCountries());
    }

    /**
     * Returns shipping rate result error method
     *
     * @param string $errorText
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error
     */
    protected function _getErrorMethod($errorText)
    {
        //Mage::logException(new Exception("Temando API Quote error : ".$errorText));
        //@todo log exception here
        $error = $this->_rateErrorFactory->create();
        $error->setCarrier(self::CARRIER_CODE);
        $error->setCarrierTitle(self::getTitle());
        if (isset($this->_errors_map[$errorText])) {
            $errorText = $this->_errors_map[$errorText];
        } else {
            $errorText = $this->_scopeConfig->getValue('carriers/temando/specificerrmsg', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }
        $error->setErrorMessage($errorText);

        return $error;
    }

    /**
     * Sets predefined errors on the error map array
     */
    protected function setErrorsMap()
    {
        $errorsMap = array();
        // add invalid suburb / postcode error
        $invalidPostCode = "The 'destinationCountry', 'destinationCode' and 'destinationSuburb' elements ";
        $invalidPostCode .= "(within the 'Anywhere' type) do not contain valid values.  ";
        $invalidPostCode .= "These values must match with the predefined settings in the Temando system.";
        $errorsMap[$invalidPostCode] = __("Invalid suburb / postcode combination.");

        $genericErrorMessage = $this->_scopeConfig->getValue('carriers/temando/specificerrmsg', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $errorsMap[self::ERR_INVALID_COUNTRY] = $genericErrorMessage;
        $errorsMap[self::ERR_INVALID_DEST] = $genericErrorMessage;
        $errorsMap[self::ERR_NO_METHODS] = $genericErrorMessage;
        $errorsMap[self::ERR_INTERNATIONAL] = $genericErrorMessage;
        $errorsMap[self::ERR_NO_ORIGIN] = $genericErrorMessage;
        // set the errors map
        $this->_errors_map = $errorsMap;
    }

    /**
     * @param $requestJson
     * @param $responseJson
     */
    private function _cacheSaveShippingOptions($request, $response)
    {
        $this->_cache->save($response, sha1($request), [self::CACHE_TAG], self::CACHE_LIFETIME);
    }

    /**
     * @param $requestJson
     * @return bool|string
     */
    private function _cacheLoadShippingOptions($request)
    {
        return $this->_cache->load(sha1($request));
    }

    /**
     * Creates a string describing the applicable elements of a rate request.
     *
     * This is used to determine if the quotes fetched last time should be
     * refreshed, or if they can remain valid.
     *
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $rateRequest
     * @param int $salesQuoteId
     *
     * @return string
     */
    protected function _createRequestString(\Magento\Quote\Model\Quote\Address\RateRequest $rateRequest, $salesQuoteId)
    {
        $requestString = $salesQuoteId . '|';
        foreach ($rateRequest->getAllItems() as $item) {
            $requestString .= $item->getProductId() . 'x' . $item->getQty();
        }

        $requestString .= '|' . $rateRequest->getDestCity();
        $requestString .= '|' . $rateRequest->getDestCountryId();
        $requestString .= '|' . $rateRequest->getDestPostcode();

        //$requestString .= '|' . $this->getDestinationType();//@todo and add unattended_delivery
        //@todo add origin
        return $requestString;
    }

}
