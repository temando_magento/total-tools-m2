<?php
namespace Temando\Temando\Model\Resource\Origin;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;
    /**
     * Define model & resource model
     */
    const TEMANDO_ORIGIN_TABLE = 'temando_origin';

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Temando\Temando\Helper\Data $helper,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_helper = $helper;
        $this->_init(
            'Temando\Temando\Model\Origin',
            'Temando\Temando\Model\Resource\Origin'
        );
        parent::__construct(
            $entityFactory, $logger, $fetchStrategy, $eventManager, $connection,
            $resource
        );
        $this->storeManager = $storeManager;
    }

    /**
     * Returns origin with which serves given postcode (if multiple results, return origin with highest id?!)
     *
     * @param string $postcode
     * @param string $country
     * @param int $storeId
     * @return Temando_Temando_Model_Warehouse|null
     */
    public function getOriginByPostcode($postcode, $countryId = null, $storeId = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $countryId = $countryId ? $countryId : $this->_helper->getDefaultCountryId();
        $this->addFieldToFilter('is_active', 1)->setOrder('origin_id', 'ASC')->load();
        $validOrigin = null;
        foreach ($this->_items as $warehouse) {
            $origin = $objectManager->create('Temando\Temando\Model\Origin');

            $origin = $origin->load($warehouse->getId());
            $store_ids = explode(',', $origin->getStoreIds());
            if ($storeId && !in_array($storeId, $store_ids)) {
                continue;
            }
        
            if ($origin->servesArea($postcode, $countryId)) {
                $validOrigin = $origin;
                break;
            }
        }
        if(!$validOrigin){
            return $origin->load(1);
        }
        return $validOrigin;
    }

}
?>