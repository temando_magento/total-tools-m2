<?php
namespace Temando\Temando\Model\Plugin;

class ShippingInformationManagementPlugin
{

    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {

        $extAtt = $addressInformation->getExtensionAttributes();
        $quote = $this->quoteRepository->getActive($cartId);

        if(!is_null($extAtt)) {
            if (!is_null($extAtt->getIsBusinessAddress())) {
                $quote->getShippingAddress()->setIsBusinessAddress($extAtt->getIsBusinessAddress());
            }
            if (!is_null($extAtt->getIsSignatureRequired())) {
                $quote->getShippingAddress()->setIsSignatureRequired($extAtt->getIsSignatureRequired());
            }
        }
    }

}
