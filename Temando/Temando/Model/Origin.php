<?php
namespace Temando\Temando\Model;
use Temando\Temando\Api\Data\OriginInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException as CoreException;
class Origin extends \Magento\Framework\Model\AbstractModel implements OriginInterface, IdentityInterface
{

    /**
     * List of countries using non-numerical postal codes
     *
     * @var array
     */
    protected $_nonNumericPcodes = array('GB', 'CA');

    /**
     * Origin's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;


    /**
     * Origin cache tag
     */
    const CACHE_TAG = 'temando_origin';

    /**
     * @var string
     */
    protected $_cacheTag = 'temando_origin';
    protected $_client;
    protected $_scopeConfig;
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'temando_origin';

    /**
     * logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    public function __construct(\Psr\Log\LoggerInterface $logger,
            \Temando\Temando\Model\Api\Client $client,
            \Magento\Framework\Model\Context $context,
            \Magento\Framework\Registry $registry,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->_logger = $logger;
        $this->_client = $client;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $registry);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Temando\Temando\Model\ResourceModel\Origin');
    }

    /**
     * Prepare origin's statuses.
     * Available event temando_origin_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ORIGIN_ID);
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Get company name
     *
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->getData(self::COMPANY_NAME);
    }

    /**
     * Get ERP ID
     *
     * @return string|null
     */
    public function getErpId()
    {
        return $this->getData(self::ERP_ID);
    }

    /**
     * Get street
     *
     * @return string|null
     */
    public function getStreet()
    {
        return $this->getData(self::STREET);
    }

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * Get region
     *
     * @return string|null
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostcode()
    {
        return $this->getData(self::POSTCODE);
    }

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * Get contact name
     *
     * @return string|null
     */
    public function getContactName()
    {
        return $this->getData(self::CONTACT_NAME);
    }

    /**
     * Get contact email
     *
     * @return string|null
     */
    public function getContactEmail()
    {
        return $this->getData(self::CONTACT_EMAIL);
    }

    /**
     * Get contact phone 1
     *
     * @return string|null
     */
    public function getContactPhone1()
    {
        return $this->getData(self::CONTACT_PHONE_1);
    }

    /**
     * Get contact phone 2
     *
     * @return string|null
     */
    public function getContactPhone2()
    {
        return $this->getData(self::CONTACT_PHONE_2);
    }

    /**
     * Get contact fax
     *
     * @return string|null
     */
    public function getContactFax()
    {
        return $this->getData(self::CONTACT_FAX);
    }

    /**
     * Get Store Ids
     *
     * @return array|null
     */
    public function getStoreIds(){
        return $this->getData(self::STORE_IDS);
    }

    /**
     * Get User Ids
     *
     * @return array|null
     */
    public function getUserIds(){
        return $this->getData(self::USER_IDS);
    }

    /**
     * Get Zone Ids
     *
     * @return array|null
     */
    public function getZoneId(){
        return $this->getData(self::ZONE_ID);
    }

    /**
     * Get contact fax
     *
     * @return string|null
     */
    public function getAccountMode()
    {
        return $this->getData(self::ACCOUNT_MODE);
    }

    /**
     * Get account sandbox
     *
     * @return string|null
     */
    public function getAccountSandbox()
    {
        return $this->getData(self::ACCOUNT_SANDBOX);
    }

    /**
     * Get account username
     *
     * @return string|null
     */
    public function getAccountUsername()
    {
        return $this->getData(self::ACCOUNT_USERNAME);
    }

    /**
     * Get account password
     *
     * @return string|null
     */
    public function getAccountPassword()
    {
        return $this->getData(self::ACCOUNT_PASSWORD);
    }

    /**
     * Get account client id
     *
     * @return string|null
     */
    public function getAccountClientId()
    {
        return $this->getData(self::ACCOUNT_CLIENT_ID);
    }



    /**
     * Set ID
     *
     * @param int $id
     * @return \Temando\Temando\Api\Data\OriginInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ORIGIN_ID, $id);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Temando\Temando\Api\Data\OriginInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set is active
     *
     * @param int|bool $is_active
     * @return \Temando\Temando\Api\Data\OriginInterface
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

    /**
     * Set company name
     *
     * @param string $company_name
     * @return \Temando\Temando\Api\Data\OriginInterface
     */
    public function setCompanyName($company_name)
    {
        return $this->setData(self::COMPANY_NAME, $company_name);
    }

    /**
     * set erp id
     *
     * @param string $erp_id
     * @return $this
     */
    public function setErpId($erp_id)
    {
        return $this->setData(self::ERP_ID, $erp_id);
    }

    /**
     * Set street
     *
     * @param string $street
     * @return $this
     */
    public function setStreet($street){
        return $this->setData(self::STREET, $street);
    }

    /**
     * Set city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city){
        return $this->setData(self::CITY, $city);
    }

    /**
     * Set region
     *
     * @param string $region
     * @return $this
     */
    public function setRegion($region){
        return $this->setData(self::REGION, $region);
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return $this
     */
    public function setPostcode($postcode){
        return $this->setData(self::POSTCODE, $postcode);
    }

    /**
     * Set country
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country){
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * Set contact name
     *
     * @param string $contact_name
     * @return $this
     */
    public function setContactName($contact_name){
        return $this->setData(self::CONTACT_NAME, $contact_name);
    }

    /**
     * Set contact email
     *
     * @param string $contact_email
     * @return $this
     */
    public function setContactEmail($contact_email){
        return $this->setData(self::CONTACT_EMAIL, $contact_email);
    }

    /**
     * Set contact phone 1
     *
     * @param string $contact_phone_1
     * @return $this
     */
    public function setContactPhone1($contact__phone_1){
        return $this->setData(self::CONTACT_PHONE_1, $contact__phone_1);
    }

    /**
     * Set contact phone 2
     *
     * @param string $contact_phone_2
     * @return $this
     */
    public function setContactPhone2($contact_phone_2){
        return $this->setData(self::CONTACT_PHONE_2, $contact_phone_2);
    }

    /**
     * Set contact fax
     *
     * @param string $contact_fax
     * @return $this
     */
    public function setContactFax($contact_fax){
        return $this->setData(self::CONTACT_FAX, $contact_fax);
    }

    /**
     * Set store ids
     *
     * @param array $store_ids
     * @return $this
     */
    public function setStoreIds($store_ids){
        return $this->setData(self::STORE_IDS, $store_ids);
    }

    /**
     * Set zone ids
     *
     * @param array $zone_ids
     * @return $this
     */
    public function setZoneId($zone_id){
        return $this->setData(self::ZONE_ID, $zone_id);
    }

    /**
     * Set user ids
     *
     * @param array $user_ids
     * @return $this
     */
    public function setUserIds($user_ids){
        return $this->setData(self::USER_IDS, $user_ids);
    }

    /**
     * Set account mode
     *
     * @param string $account_mode
     * @return $this
     */
    public function setAccountMode($account_mode){
        return $this->setData(self::ACCOUNT_MODE, $account_mode);
    }

    /**
     * Set account sandbox
     *
     * @param boolean $account_sandbox
     * @return $this
     */
    public function setAccountSandbox($account_sandbox){
        return $this->setData(self::ACCOUNT_SANDBOX, $account_sandbox);
    }

    /**
     * Set user ids
     *
     * @param string $account_username
     * @return $this
     */
    public function setAccountUsername($account_username){
        return $this->setData(self::ACCOUNT_USERNAME, $account_username);
    }

    /**
     * Set account password
     *
     * @param string $account_password
     * @return $this
     */
    public function setAccountPassword($account_password){
        return $this->setData(self::ACCOUNT_PASSWORD, $account_password);
    }

    /**
     * Set account client id
     *
     * @param string $account_client_id
     * @return $this
     */
    public function setAccountClientId($account_client_id){
        return $this->setData(self::ACCOUNT_CLIENT_ID, $account_client_id);
    }
    
    /**
     * Try to sync the warehouse with the Temando API
     * If the location doesn't exist it will be created
     * otherwise it will be updated
     */
    public function syncWarehouse()
    {
        $request['location'] = $this->toCreateLocationRequestArray();
        try {
            $api = $this->_client->connect($this->getTemandoProfile());
            $result = $api
                ->getLocations(
                    array(
                        'type' => 'Origin',
                        'clientId' => $this->getAccountClientid(),
                        'description' => $this->getName()
                    )
                );
            if ($result && isset($result->locations->location)) {
                //location exists = update
                $result = $api->updateLocation($request);
            } else {
                $result = $api->createLocation($request);
            }
        } catch (Exception $e) {
            $this->_logger->critical($e);
        }
        return $result;
    }

    /**
     * Returns Temando Account details
     *
     * @return array
     */
    public function getTemandoProfile()
    {
        if ($this->getAccountMode()) {
            return array(
                'sandbox'   => $this->getAccountSandbox(),
                'clientid'  => $this->getAccountClientid(),
                'username'  => $this->getAccountUsername(),
                'password'  => $this->getAccountPassword(),
                'country'   => $this->getCountry(),
            );
        } else {
            return array(
                'sandbox'   => $this->_scopeConfig->getValue('temando/general/sandbox', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'clientid'  => $this->_scopeConfig->getValue('temando/general/client', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'username'  => $this->_scopeConfig->getValue('temando/general/username', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'password'  => $this->_scopeConfig->getValue('temando/general/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'country'   => $this->getCountry(),
            );
        }
    }
    
    /**
     * Returns request array used to create location via API
     *
     * @return array
     */
    public function toCreateLocationRequestArray()
    {
        return array(
            'description' => $this->getName(),
            'type' => 'Origin',
            'contactName' => $this->getContactName(),
            'companyName' => $this->getCompanyName(),
            'street' => $this->getStreet(),
            'suburb' => $this->getCity(),
            'state' => $this->getRegion(),
            'code' => $this->getPostcode(),
            'country' => $this->getCountry(),
            'phone1' => preg_replace('/\D/', '', $this->getData('contact_phone_1')),
            'phone2' => preg_replace('/\D/', '', $this->getData('contact_phone_2')),
            'fax' => $this->getContactFax(),
            'email' => $this->getContactEmail(),
            'loadingFacilities' => $this->getLoadingFacilities(),
            'forklift' => $this->getForklift(),
            'dock' => $this->getDock(),
            'limitedAccess' => $this->getLimitedAccess(),
        );
    }


    /**
     * Returns true if warehouse serves given postal code, false otherwise.
     *
     * @param string $countryId
     * @param string|int $postcode
     * @return boolean
     */
    public function servesArea($postcode, $countryId)
    {
        $zones = explode(',', $this->getZoneId());
        if (is_array($zones) && !empty($zones)) {
            foreach ($zones as $zoneId) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $zone = $objectManager->create('Temando\Temando\Model\Zone');
                $zone = $zone->load($zoneId);

                /* @var $zone \Temando\Temando\Model\Zone */

                if ($zone->getCountryCode() != $countryId) {
                    continue;
                }
                $ranges = explode(',', $zone->getRanges());

                if (count($ranges) === 1) {
                    $range = trim($ranges[0]);
                    if (empty($range)) {
                        //range not specified - all country
                        return true;
                    }
                }

                /**
                 * Spaces in non-numerical postal codes are significant!
                 * 'M1 1A' != 'M11 A' - these can be two different zones
                 */
                if (in_array(strtoupper($countryId), $this->_nonNumericPcodes)) {
                    //non-numeric postal codes (wildcart * or exact match only)
                    foreach ($ranges as $range) {
                        $beginsWith = stristr($range, '*', true);
                        if ($beginsWith) {
                            //wildcart used
                            if (stripos($postcode, $beginsWith) === 0) {
                                return true;
                            }
                        } else {
                            //exact match
                            if (strtolower($postcode) == strtolower($range)) {
                                return true;
                            }
                        }
                    }
                } else {
                    //numeric postal codes
                    foreach ($ranges as $range) {
                        $minmax = explode(':', $range);
                        if (count($minmax) == 2) {
                            //range specified as a:b
                            if ($postcode >= $minmax[0] && $postcode <= $minmax[1]) {
                                return true;
                            }
                        } elseif (count($minmax) == 1) {
                            //single value
                            if ($postcode == $minmax[0]) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }    
}