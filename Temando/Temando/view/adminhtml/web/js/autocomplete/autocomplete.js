define([
    'jquery'
], function($) { 
    var xhr;
    //var baseurl = $('#baseurl').val();

    // get country is default country
    var country =  $("select[id$='_country']").val();
    $.widget('temando.temando', {        
        _create: function() {
            var baseurl = this.options.baseUrl;
            var editFormSelector = this.options.editFormSelector;
            // Disable autofill from web browser
            $("input[id$='_city']").attr('autocomplete', 'off');
            $("input[id$='_postcode']").attr('autocomplete', 'off');
            // autocomplete 
            this.element.on('click', function(e) {
                if (e.target.getAttribute('class') === 'location') {
                    $("input[id$='_city']").val(e.target.getAttribute('suburb'));
                    $("input[id$='_postcode']").val(e.target.getAttribute('postcode'));
                    $("#autocomplete-suggesstion").remove();
                }
            });
            
            // trigger onchange event on country select field
            this.element.on("change", function(e) {
                if (e.target.getAttribute('id').indexOf('country') > 0) {
                    country = $("select[id$='_country']").val();
                    $("#autocomplete-suggesstion").remove();
                }
            });
            // trigger keyup event on postcode and suburb field
            this.element.on('keyup', function(e) {      
               // only city and postcode input are triggered keyup event
                if (e.target.getAttribute('id').indexOf('city') > 0 || e.target.getAttribute('id').indexOf('postcode') > 0) {
                    if ($("#autocomplete-suggesstion")) {
                        $("#autocomplete-suggesstion").remove();
                    }
                    //var regex = new RegExp("^[a-zA-Z0-9]+$");
                    var regex = new RegExp("^[a-zA-Z0-9\\-\\s]+$");
                   // suburb can be the suburb or postcode 
                    suburb = $("#" + e.target.getAttribute('id')).val();
                    if (regex.test(suburb) && e.keyCode != 17) {
                        var param = 'q=' + suburb + '&country='+country;
                        if (xhr && xhr.readystate != 4) {
                            xhr.abort();
                        }
                        xhr = $.ajax({
                            showLoader: true,
                            url: baseurl + 'temando/api/pcs',
                            data: param,
                            type: "POST",
                            dataType: 'json'
                        }).done(function (data) {
                            //locations = data['data'];
                            //locations = data;
                            if (data.length > 0) {
                                suggestionDiv = '';
                                var top = $('#' + e.target.getAttribute('id')).offset().top + 35;
                                var left = $('#' + e.target.getAttribute('id')).offset().left;
                                suggestionDiv = "<div id='autocomplete-suggesstion' style='top:" + top + "px; left:" + left + "px'>"; 
                                $.each (data, function(dataIndex, location) {
                                    $.each (location, function (locationsIndex, l) {
                                        suggestionDiv += "<div class='location' postcode='" + l.postcode + "' suburb='" + l.city + "'>" + l.fulltext + "</div>";
                                    });
                                });
                                suggestionDiv += "</div>";
                                $(editFormSelector).append(suggestionDiv);
                            }   
                        });  
                    }
                }
            });
        }
    });
  return $.temando.temando;
});
