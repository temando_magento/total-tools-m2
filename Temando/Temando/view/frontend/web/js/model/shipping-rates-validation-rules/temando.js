/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [],
    function () {
        "use strict";
        return {
            getRules: function() {
                return {
                    'postcode': {
                        'required': true
                    },
                    'country_id': {
                        'required': true
                    },
                    'region_id': {
                        'required': false
                    },
                    'city': {
                        'required': true
                    },
                    // 'telephone': {
                    //     'required': true
                    // },
                    // 'firstname': {
                    //     'required': true
                    // },
                    // 'lastname': {
                    //     'required': true
                    // },
                    // 'region': {
                    //     'required': false
                    // },
                    // 'company': {
                    //     'required': false
                    // },
                    // 'street': {
                    //     'required': true
                    // },
                    // 'email': {
                    //     'required': false
                    // },
                    'signature-required':
                    {
                        'required': false
                    },
                    'business-address':
                    {
                        'required': false
                    }
                };
            }
        };
    }
);
