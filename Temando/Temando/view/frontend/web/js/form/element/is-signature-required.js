/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'mageUtils'

], function (_, registry, FormElement, utils) {
    'use strict';

    var elementName = 'is-signature-required';

    return FormElement.extend({
        defaults: {
            imports: {
                update: '${ $.parentName }.country_id:value'
            },
            inputName: 'signature-required'
        },

        /**
         * @param {String} value
         */
        update: function (value) {

            var country = registry.get(this.parentName + '.' + 'country_id'),
                options = country.indexedOptions,
                option;

            if (!value) {
                return;
            }

            option = options[value];

            if (!option['is_region_required']) {
                this.error(false);
                this.validation = _.omit(this.validation, 'required-entry');
            } else {
                this.validation['required-entry'] = true;
            }

            this.required(!!option['is_region_required']);
        },

        // Overwirte initConfig to use static names
        initConfig: function () {
            var uid = utils.uniqueid(),
                scope;

            this._super();

            scope   = this.dataScope;

                _.extend(this, {
                    uid: uid,
                    noticeId: 'notice-' + uid,
                    inputName: elementName
                });

            return this;
        }
    });
});
