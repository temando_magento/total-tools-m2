define([
    "jquery",
    "Temando_Temando/js/model/shipping-rate-processor/new-address",
    "Magento_Checkout/js/model/quote",
    "jquery/ui"
], function($, newAddress, quote) {
    "use strict";
    var xhr;
    var baseUrl;
    var isLoading;

    $.widget('temando.autocomplete', {
        _create: function() { },
        initContainer: function (elem) {
            baseUrl = $("input[name='baseUrl']").val();
            $('#checkout').on('keyup', this.keyUpHandler);
            $('#address_autocomplete').on('click', this.clickHandler);
        },

        clickHandler: function (e) {
            if (e.target.getAttribute('class') === 'location') {
                $("input[name='city']").val(e.target.getAttribute('data-city'));
                $("input[name='postcode']").val(e.target.getAttribute('data-postcode'));
                $("#loadingAddress").val(0);
                quote.shippingAddress().city = e.target.getAttribute('data-city');
                quote.shippingAddress().postcode = e.target.getAttribute('data-postcode');
                newAddress.getRates(quote.shippingAddress());
                $("#address_autocomplete").empty();
                $("#address_autocomplete").hide();
            } else {
                $("#address_autocomplete").empty();
                $("#address_autocomplete").hide();
            }
        },
        keyUpHandler: function (e) {
            if (e.target.getAttribute('type') === 'text') {
                if ((e.target.attributes.name.value == 'postcode') ||
                    (e.target.attributes.name.value == 'city'))
                {
                    $("#loadingAddress").val(1);
                    $("#address_autocomplete").hide();
                    $("#address_autocomplete").empty();
                    var top = $('#' + e.target.getAttribute('id')).offset().top + 35;
                    var left = $('#' + e.target.getAttribute('id')).offset().left;
                    var q = '';
                    var country = $('select[name="country_id"]').val();
                    if(e.target.attributes.name.value == 'postcode'){
                        q = $('input[name="postcode"]').val();
                    } else if(e.target.attributes.name.value == 'city'){
                        q = $('input[name="city"]').val();
                    }
                    //ajax lookup
                    var param = 'q=' + q + '&country='+country;
                    if (xhr && xhr.readystate != 4) {
                        xhr.abort();
                    }
                    xhr = $.ajax({
                        showLoader: true,
                        url: baseUrl + 'temando/api/pcs',
                        data: param,
                        type: "POST",
                        dataType: 'json'
                    }).done(function (data) {
                        var dropdown = $('<ul role="listbox"></ul>');
                        var html;
                        if (data.length > 0) {
                            $.each (data, function(dataIndex, location) {
                                $.each(location, function (locationsIndex, l) {
                                    if(typeof(l.fulltext) != "undefined" && l.fulltext !== null){
                                        html = '<li class="location" data-city="' + l.city + '" data-postcode="' + l.postcode + '">' + l.fulltext + '</li>';
                                        dropdown.append(html);
                                    } else {
                                        html = '<li>Suburb / Postcode not found</li>';
                                        dropdown.append(html);
                                    }
                                });
                            });
                        }

                        $("#address_autocomplete").append(dropdown);
                        $("#address_autocomplete").css('top', top+'px');
                        $("#address_autocomplete").css('left', left+'px');
                        $("#address_autocomplete").show();
                    });
                }
            }
        }
    });
    return $.temando.autocomplete;
});