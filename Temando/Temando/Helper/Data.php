<?php

namespace Temando\Temando\Helper;

use Magento\Catalog\Model\Product;
use \Temando\Temando\Model\Zone;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\User\Model\ResourceModel\User\Collection
     */
    protected $_userCollection;
    /**
     * @var \Temando\Temando\Model\ResourceModel\Zone\Collection
     */
    protected $_zoneCollection;
    /**
     * @var \Temando\Temando\Model\ResourceModel\Origin\Collection
     */
    protected $_originCollection;

    /**
     * @var \Temando\Temando\Model\Zone
     */
    protected $_zone;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    /*
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_countryHelper;
    /*
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;
    
    /**
     * @var \Temando\Temando\Model\System\Config\Source\Unit\Weight
    */
    protected $_weight;
   
    /**
     * @var \Temando\Temando\Model\System\Config\Source\Unit\Measure
    */
    protected $_measure;

    /**
     * Default currency code
     */
    const DEFAULT_CURRENCY_CODE = 'AUD';

    /**
     * Default country id
     */
    const DEFAULT_COUNTRY_ID = 'AU';
    
    public function __construct(
        \Magento\User\Model\ResourceModel\User\Collection $userCollection,
        \Magento\Catalog\Model\Product $product,
        \Temando\Temando\Model\ResourceModel\Zone\Collection $zoneCollection,
        \Temando\Temando\Model\ResourceModel\Origin\Collection $originCollection,
        Zone $zone,
        \Magento\Backend\Block\Template\Context $templateContext,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Directory\Model\Config\Source\Country $countryHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Temando\Temando\Model\System\Config\Source\Unit\Weight $weight,    
        \Temando\Temando\Model\System\Config\Source\Unit\Measure $measure
    )
    {
        $this->_userCollection = $userCollection;
        $this->_product = $product;
        $this->_zoneCollection = $zoneCollection;
        $this->_originCollection = $originCollection;
        $this->_zone = $zone;
        $this->_authorization = $templateContext->getAuthorization();
        $this->_authSession = $authSession;
        $this->_countryHelper = $countryHelper;
        $this->_scopeConfig = $scopeConfig;
        $this->_logger = $logger;
        $this->_storeManager = $storeManager;
        $this->_priceCurrency = $priceCurrency;
        $this->_weight = $weight;
        $this->_measure = $measure;
    }
    /**
     * Get zones Options Array
     *
     * @return array
     */
    public function getZonesOptionArray()
    {
        $zones = array();
        $zoneItems = $this->_zoneCollection->load();
        foreach($zoneItems AS $zone){
            $zones[$zone->getId()] = $zone->getData('name');
        }
        return $zones;
    }

    /**
     * Get users Options Array
     *
     * @return array
     */
    public function getUsersOptionArray(){
        $users = array();
        $userItems = $this->_userCollection->load();
        foreach($userItems AS $user) {
            $users[] = [
                'value' => $user->getId(),
                'label' => $user->getData('username')
            ];
        
        }
        return $users;

    }

    /**
     * Get origins Options Array
     *
     * @return array
     */

    public function getOriginOptionArray(){
        $origins = array();
        $originItems = $this->_originCollection->load();

        $user = $this->_authSession->getUser();

        foreach($originItems AS $origin) {
            $originUserIds = explode(',', $origin->getUserIds());
            if (($user->getAclRole() == 1) || (in_array($user->getUserId(), $originUserIds))) {
                $origins[$origin->getId()] = $origin->getData('name');
            }
        }
        return $origins;
    }

    /**
     * get catalog product from
     * @param \Temando\Temando\Model\Shipment\Item $item
     * @return bool|\Magento\Catalog\Model\AbstractModel
     */
    public function getCatalogProduct(\Temando\Temando\Model\Shipment\Item $item){
        return $this->_product->loadByAttribute('sku', $item->getSku());
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    public function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Returns articles of an item
     * (supports scenario where 1 sku is shipped in up to 5 packages)
     *
     * @param \Magento\Sales\Model\Quote\Item $item
     * @return array
     */
    public function getProductArticles($item, $international = false)
    {
        $articles = array();
        $this->_isInternational = $international;

        $product = $this->_product->loadByAttribute('sku', $item->getSku());

        $weight = $item->getWeight();
        $length = $product->getData("shipping_length");
        $width = $product->getData("shipping_width");
        $height = $product->getData("shipping_height");
            
        $articles[] = array(
            'description' => $this->cleanArticleDescription($item->getName()),
            'packaging' => 'Box',
            'value' => round($item->getPrice(), 2),
            'fragile' => $product->getData("shipping_fragile") ? $product->getData("shipping_fragile") : $this->_scopeConfig->getValue('temando/defaults/fragile', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'dangerous' => $product->getData("shipping_dangerous") ? $product->getData("shipping_dangerous") : $this->_scopeConfig->getValue('temando/defaults/dangerous', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'needs_packaging' => $product->getData("shipping_needs_packaging"),
            'weight' => $weight ? $weight : $this->_scopeConfig->getValue('temando/defaults/weight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'length' => $length ? $length : $this->_scopeConfig->getValue('temando/defaults/length', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'width' => $width ? $width : $this->_scopeConfig->getValue('temando/defaults/width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'height' => $height ? $height : $this->_scopeConfig->getValue('temando/defaults/height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
        );
        return $articles;
    }

    /*
     * Clean the article description from unwanted characters before passing
     * the article array
     *
     * @param string $description
     * @return string
     */
    public function cleanArticleDescription($description)
    {
        $description = str_replace(',', '', $description);
        $description = str_replace('"', '', $description);
        $description = str_replace("'", '', $description);
        return $description;
    }

    /**
     * format price
     *
     * @param $price
     * @return mixed
     */
    public function formatPrice($price){
        return number_format($price, 2, '.', ',');
    }

    /**
     * check shipment permission
     *
     * @param $shipment
     * @return bool
     */
    public function checkShipmentPermission(\Temando\Temando\Model\Shipment $shipment){
        $origin = $shipment->getOrigin();
        $user = $this->_authSession->getUser();
        $originUserIds = explode(',', $origin->getUserIds());

        if (($user->getAclRole() == 1) || (in_array($user->getId(), $originUserIds))) {
            return true;
        }
        return false;
    }

    
    /**
     * Gets shipment currency code (by order store)
     *
     * @return string
     */

    public function getStoreCurrencyCode()
    {
        return $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
    }
    
    /**
    * Get current currency symbol
    *
    * @return string
    */ 
   public function getCurrentCurrencySymbol()
   {
        return $this->_priceCurrency->getCurrency()->getCurrencySymbol();
   }
   
    /**
     * Return the weight unit text
     * @param int $unit
     * @return string
     */
    public function getWeightUnitText($unit = null)
    {
        if (!$unit) {
            $unit = $this->_scopeConfig->getValue('temando/units/weight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }
        return $this->_weight->getBriefOptionLabel($unit);
    }

    /**
     * Return the measure unit text
     * @param int $unit
     * @return string
     */
    public function getMeasureUnitText($unit = null)
    {
        if (!$unit) {
            $unit = $this->_scopeConfig->getValue('temando/units/measure', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }
        return $this->_measure->getBriefOptionLabel($unit);
    }

    /**
     * Get Carrier object by specific temando carrier id
     * 
     * @param int $temandoCarrierId
     * @return \Temando\Temando\Model\Carrier
     */
    public function getCarrierByTemandoId($temandoCarrierId) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $carrierCollection = $objectManager->create('Temando\Temando\Model\ResourceModel\Carrier\Collection');
        return $carrierCollection->addFieldToFilter('temando_carrier_id', $temandoCarrierId)->load()->getFirstItem();
    }

    
    /**
     * Get product weight
     * @param \Magento\Catalog\Product $product
     * @return float
     */
    public function getItemWeight($product){
        return $product->getWeight() ? $product->getWeight() : $this->_scopeConfig->getValue('temando/defaults/weight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * Get product width
     * @param \Magento\Catalog\Product $product
     * @return float
     */
    public function getItemWidth($product){
        return $product->getShippingWidth() ? $product->getShippingWidth() : $this->_scopeConfig->getValue('temando/defaults/width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * Get product length
     * @param \Magento\Catalog\Product $product
     * @return float
     */
    public function getItemLength($product){
        return $product->getShippingLength() ? $product->getShippingLength() : $this->_scopeConfig->getValue('temando/defaults/length', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * Get product height
     * @param \Magento\Catalog\Product $product
     * @return float
     */
    public function getItemHeight($product)
    {
        return $product->getShippingHeight() ? $product->getShippingHeight() : $this->_scopeConfig->getValue('temando/defaults/height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }
    /**
     * Returns array of allowed countries based on Magento system configuration
     * and Temando plugin allowed countries.
     *
     * @param boolean $asJson
     * @return array
     */
    public function getAllowedCountries($asJson = false)
    {
        $specific = $this->_scopeConfig->getValue('carriers/temando/sallowspecific', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        //check if all allowed and return selected
        if ($specific == 1) {
            $availableCountries = explode(',', $this->_scopeConfig->getValue('carriers/temando/specificcountry', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        } else {
            //return all allowed
            $availableCountries = explode(',', $this->_scopeConfig->getValue('general/country/allow', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        }

        $countries = array_intersect_key($this->getAllCountries(), array_flip($availableCountries));

        if ($asJson) {
            return json_encode($countries);
        }
        return $countries;
    }

    /**
     * Returns an array of all Magento countries
     *
     * @return array
     */
    public function getAllCountries()
    {
        //$countries = array();
        $countries = $this->_countryHelper->toOptionArray();
        //foreach ($countryCollection as $country) {
//        foreach ( $countries as $countryKey => $country ) {
//                /* @var $country Mage_Directory_Model_Country */
//                $countries[$country['value']] = $country->getName();
//            }
//        }
        return $countries;

    }

    /**
     * Gets the date when a package will be ready to ship. Adjusts dates so
     * that they always fall on a weekday.
     *
     * @param <type> $ready_time timestamp for when the package will be ready
     * to ship, defaults to 10 days from current date
     */
    public function getReadyDate($ready_time = null)
    {
        if (is_null($ready_time)) {
            $ready_time = strtotime('+10 days');
        }
        if (is_numeric($ready_time) && $ready_time >= strtotime(date('Y-m-d'))) {
            $weekend_days = array('6', '7');
            while (in_array(date('N', $ready_time), $weekend_days)) {
                $ready_time = strtotime('+1 day', $ready_time);
            }
            return $ready_time;
        }
    }

    /**
     * Converts given weight from configured unit to grams
     *
     * @param float $value Weight to convert
     * @return float Converted weight in grams
     */
    public function getWeightInGrams($value, $currentUnit = null)
    {
        $value = floatval($value);
        $currentUnit = $currentUnit ? $currentUnit : $this->_scopeConfig->getValue('temando/units/weight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        //from units as specified in configuration
        switch($currentUnit) {
            case \Temando\Temando\Model\System\Config\Source\Unit\Weight::KILOGRAMS:
                return $value * 1000;
                break;

            case \Temando\Temando\Model\System\Config\Source\Unit\Weight::OUNCES:
                return $value * 28.3495;
                break;

            case \Temando\Temando\Model\System\Config\Source\Unit\Weight::POUNDS:
                return $value * 453.592;
                break;

            default:
                return $value;
                break;
        }
    }


    /**
     * Converts given distance from configured unit to centimetres
     *
     * @param float $value Distance to convert
     * @return float Converted distance in centimetres
     */
    public function getDistanceInCentimetres($value, $currentUnit = null)
    {
        $value = floatval($value);
        $currentUnit = $currentUnit ? $currentUnit : $this->_scopeConfig->getValue('temando/units/measure', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        switch($currentUnit) {
            case \Temando\Temando\Model\System\Config\Source\Unit\Measure::METRES:
                return $value * 100;
                break;

            case \Temando\Temando\Model\System\Config\Source\Unit\Measure::FEET:
                return $value * 30.48;
                break;

            case \Temando\Temando\Model\System\Config\Source\Unit\Measure::INCHES:
                return $value * 2.54;
                break;

            default:
                return $value;
                break;
        }

    }

    /**
     * Returns default currency code
     */
    public function getDefaultCurrencyCode()
    {
        return self::DEFAULT_CURRENCY_CODE;
    }

    /**
     * Returns default country id
     */
    public function getDefaultCountryId()
    {
        return self::DEFAULT_COUNTRY_ID;
    }

    /**
     * Packaging Logic Algorithm
     * Decide how best to package the cart items into boxes, consolidate items into boxes as per product attributes
     *
     * @param $order
     * @return array
     */

    public function calculateBoxes($allItems){

        $articleCount = 0;
        $boxesRequired = array();
        $noPackingArticles = array();

        $currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();

        foreach ($allItems as $item) {
            $product = $this->_product->load($item->getProductId());

            if ($item->getParentItem() || $item->getIsVirtual()) {
                // do not add child products or virtual items
                continue;
            }

            if ($item->getProduct() && $item->getProduct()->isVirtual()) {
                // do not add virtual product
                continue;
            }

            if ($item->getFreeShipping()) {
                continue;
            }

            foreach ($this->getProductArticles($item) AS $article){
                if(!$article['needs_packaging']){
                    $qty = $item->getQty() ? $item->getQty() : $item->getQtyOrdered();
                    for ($i=0; $i<=$qty; $i++) {
                        $qty = $item->getQty() ? $item->getQty() : $item->getQtyOrdered();
                        $articles = array();
                        for ($i = 1; $i <= $qty; $i++) {
                            //explicitly declare the single article in a box
                            $a = array();

                            $a['comment']   = $article['description'];
                            $a['qty']       = 1;
                            $a['value']     = $article['value'];
                            $a['length']    = $article['length'];
                            $a['width']     = $article['width'];
                            $a['height']    = $article['height'];
                            $a['weight']    = $article['weight'];
                            $a['fragile']   = $article['fragile'];
                            $a['dangerous'] = $article['dangerous'];
                            //populate articles
                            $articleItems = array(
                                'description'   => $article['description'],
                                'sku'           => $product->getSku(),
                                'goodsValue'    => $article['value'],
                                'goodsCurrency' => $currencyCode
                            );

                            $a['articles'][]  = $articleItems;

                            $boxesRequired[] = $a;
                        }
                    }
                } else {

                    $a['comment']   = $article['description'];
                    $a['qty']       = $item->getQty() ? $item->getQty() : $item->getQtyOrdered();
                    $a['value']     = $article['value'];
                    $a['length']    = $article['length'];
                    $a['width']     = $article['width'];
                    $a['height']    = $article['height'];
                    $a['weight']    = $article['weight'];
                    $a['fragile']   = $article['fragile'];
                    $a['dangerous'] = $article['dangerous'];
                    //populate articles
                    $articleItems = array(
                        'description'   => $article['description'],
                        'sku'           => $product->getSku(),
                        'goodsValue'    => $article['value'],
                        'goodsCurrency' => $currencyCode
                    );
                    $a['articles'][]= $articleItems;

                    $noPackingArticles[] = $a;
                }
                $articleCount++;
            }
        }

        //check if we can consolidate boxes

        if(count($boxesRequired)){
            //at least one item needs a box
            $articleItems = array();
            if(count($noPackingArticles) > 0) {
                //stick the $noPackingArticles in the first box
                $firstBox           = $boxesRequired[0];
                $firstBoxComment    = $firstBox['comment'];
                $firstBoxValue      = $firstBox['value'];
                $firstBoxWeight     = $firstBox['weight'];
                $firstBoxArticles   = $firstBox['articles'][0];
                $articleNames       = array();
                $articleItems[]      = $firstBox['articles'][0];//array();
                foreach($noPackingArticles AS $noPackArticleIndex=>$noPackArticle){
                    //need qty for loop here
                    $qty = $noPackArticle['qty'];
                    for($i=0; $i<$qty; $i++){
                        $firstBoxComment    .= ', ' . $noPackArticle['comment'];
                        $firstBoxValue      = $firstBoxValue + $noPackArticle['value'];
                        $firstBoxWeight     = $firstBoxWeight + $noPackArticle['weight'];

                        //consolidate articles
                        $articleItems[] = $noPackArticle['articles'][0];

                    }

                    if($noPackArticle['fragile']){
                        $firstBox['fragile']    = 1;
                    }
                    if($noPackArticle['dangerous']){
                        $firstBox['dangerous']  = 1;
                    }
                }
                $firstBox['comment']    = $firstBoxComment;
                $firstBox['value']      = $firstBoxValue;
                $firstBox['weight']     = $firstBoxWeight;
                //populate articles
                $firstBox['articles']   = $articleItems;
                $boxesRequired[0]       = $firstBox;

            }
        } else {
            $articleNames   = array();
            $articleItems   = array();
            //no items specifically need a box
            if(count($noPackingArticles)==1){
                //single box required
                $noPackArticle = $noPackingArticles[0];
                $comment        = '';
                $value          = 0;
                $weight         = 0;

                $qty = $noPackArticle['qty'];
                for($i=0; $i<$qty; $i++){
                    $comment .= $noPackArticle['comment'];
                    if($i<($qty-1)){
                        $comment .= ', ';
                    }
                    $value = $value + $noPackArticle['value'];
                    $weight = $weight + $noPackArticle['weight'];
                    //consolidate articles
                    $articleItems[]   = $noPackArticle['articles'][0];
                }
                $noPackArticle['comment']   = $comment;
                $noPackArticle['value']     = $value;
                $noPackArticle['weight']    = $weight;
                //populate articles
                $noPackArticle['articles']  = $articleItems;
                $boxesRequired[0]           = $noPackArticle;
            } elseif(count($noPackingArticles) > 1){
                //all articles in one box
                $comment        = '';
                $value          = 0;
                $length         = 0;
                $width          = 0;
                $height         = 0;
                $weight         = 0;
                $fragile        = false;
                $dangerous      = false;

                $count = 0;

                foreach($noPackingArticles AS $noPackArticleIndex=>$noPackArticle){

                    $qty = $noPackArticle['qty'];
                    for($i=0; $i<$qty; $i++){
                        $comment .= $noPackArticle['comment'];
                        if($i<($qty-1)){
                            $comment .= ', ';
                        }
                        $value = $value + $noPackArticle['value'];
                        $weight = $weight + $noPackArticle['weight'];
                        //consolidate articles
                        $articleItems[]   = $noPackArticle['articles'];
                    }
                    if($count < count($noPackingArticles)-1){
                        $comment .= ', ';
                    }
                    if($noPackArticle['length'] > $length){
                        $length = $noPackArticle['length'];
                    }
                    if($noPackArticle['width'] > $width){
                        $width = $noPackArticle['width'];
                    }
                    if($noPackArticle['height'] > $height){
                        $height = $noPackArticle['height'];
                    }
                    if($noPackArticle['fragile']){
                        $fragile = true;
                    }
                    if($noPackArticle['dangerous']){
                        $dangerous = true;
                    }
                    $count++;
                }
                $firstBox               = array();
                $firstBox['comment']    = $comment;
                $firstBox['qty']        = 1;
                $firstBox['value']      = $value;
                $firstBox['length']     = $length;
                $firstBox['width']      = $width;
                $firstBox['height']     = $height;
                $firstBox['weight']     = $weight;
                $firstBox['fragile']    = $fragile;
                $firstBox['dangerous']  = $dangerous;
                //populate articles
                $firstBox['articles']   = $articleItems;

                $boxesRequired[] = $firstBox;
            }
        }
        return $boxesRequired;
    }
}