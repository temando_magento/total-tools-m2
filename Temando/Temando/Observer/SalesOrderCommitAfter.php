<?php

namespace Temando\Temando\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\DataObject as Object;
use Temando\Temando\Model\Shipping\Carrier\Temando;

class SalesOrderCommitAfter implements ObserverInterface
{
    /**
     * Quote Repository
     *
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $_quoteRepository;

    /**
     * Temando shipment
     *
     * @var \Temando\Temando\Model\Shipment
     */
    protected $_shipment;

    /**
     * Temando pickup
     *
     * @var \Temando\Temando\Model\Pickup
     */
    protected $_pickup;

    /**
     * Temando box
     *
     * @var \Temando\Temando\Model\Box
     */
    protected $_box;

    /**
     * @var \Magento\Catalog\Product
     */
    protected $_product;

    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Store\Model\ScopeInterface
     */
    protected $_scopeConfig;

    /**
     *
     */
    const PICKUP_SHIPPING_METHOD_PREFIX = 'storepickup_';

    /**
     *
     */
    const TEMANDO_SHIPPING_METHOD_PREFIX = 'temando_';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Temando\Temando\Model\Resource\Origin\Collection
     */
    protected $_originCollection;

    /**
     * @var \Temando\Temando\Model\Quote
     */
    protected $_quote;

    /**
     * SalesOrderCommitAfter constructor.
     * @param \Temando\Temando\Model\Shipment $shipment
     * @param \Temando\Temando\Model\Box $box
     * @param \Temando\Temando\Helper\Data $helper
     * @param \Temando\Temando\Model\Resource\Origin\Collection $originCollection
     * @param \Temando\Temando\Model\Quote $quote
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Temando\Temando\Model\Shipment $shipment,
//        \Temando\Temando\Model\Pickup $pickup,
        \Temando\Temando\Model\Box $box,
        \Temando\Temando\Helper\Data $helper,
        \Temando\Temando\Model\Resource\Origin\Collection $originCollection,
        \Temando\Temando\Model\Quote $quote,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Psr\Log\LoggerInterface $logger
    )
    {

        $this->_shipment = $shipment;
//        $this->_pickup = $pickup;
        $this->_box = $box;
        $this->_helper = $helper;
        $this->_originCollection = $originCollection;
        $this->_quote = $quote;
        $this->_scopeConfig = $scopeConfig;
        $this->_product = $product;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteRepository = $quoteRepository;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Temando\Temando\Model\Shipment
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //$order = $observer->getEvent()->getOrder();
//        if($order->getShippingMethod().stringStartsWith(self::PICKUP_SHIPPING_METHOD_PREFIX)){
        // $this->createTemandoPickup($observer);
//        } else if($order->getShippingMethod().stringStartsWith(self::TEMANDO_SHIPPING_METHOD_PREFIX)){
        //$this->updateAdditionalAddressData($observer);
        $this->createTemandoShipment($observer);
//        }
        $this->_checkoutSession->setTemandoRequestString(null);
        return;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Temando\Temando\Model\Pickup
     */
    public function createTemandoPickup(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        //$shippingAddress = $order->getShippingAddress();
        $this->_logger->debug('create temando pickup');

        //$this->_logger->debug(print_r(get_class_methods($order), 1));
        $this->_logger->debug('SHIPPING METHOD:' . ($order->getShippingMethod()));
        $billingAddress = $order->getBillingAddress();

        $this->_pickup->setData('order_id', $order->getId());
        $this->_pickup->setData('order_increment_id', $order->getIncrementId());

        $this->_pickup->setData('origin_id', 1);
        $this->_pickup->setData('customer_selected_origin', 1);
        $this->_pickup->setData('status', 0);//@todo should read from class variable
        $this->_pickup->setData('billing_contact_name', $billingAddress->getName());
        $this->_pickup->setData('billing_company_name', $billingAddress->getCompany());
        $streetAddress = $billingAddress->getStreetLine(1);
        if ($billingAddress->getStreetLine(2)) {
            $streetAddress .= ", " . $billingAddress->getStreetLine(2);
        }
        $this->_pickup->setData('billing_street', $streetAddress);
        $this->_pickup->setData('billing_city', $billingAddress->getCity());
        $this->_pickup->setData('billing_postcode', $billingAddress->getPostcode());
        $this->_pickup->setData('billing_region', $billingAddress->getRegion());
        $this->_pickup->setData('billing_country', $billingAddress->getCountryId());
        $this->_pickup->setData('billing_phone', $billingAddress->getTelephone());
        $this->_pickup->setData('billing_email', $order->getCustomerEmail());

        try {
            $this->_pickup->save();
        } catch (\Exception $e) {
            $this->_logger->debug(__('Failed to save pickup') . ' ' . $e->getMessage());
        }
        $this->clearSessionData();

        return $this->_pickup;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Temando\Temando\Model\Shipment
     */
    public function createTemandoShipment(\Magento\Framework\Event\Observer $observer)
    {

        $order = $observer->getEvent()->getOrder();
        $shippingAddress = $order->getShippingAddress();

        $shippingMethod = $order->getShippingMethod();
        $this->_logger->debug(get_class($this) . ' Shipping Method  : ' . $shippingMethod);

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_quoteRepository->get($order->getQuoteId());
        $quoteShippingAddress = $quote->getShippingAddress();

        $origin = $this->_originCollection->getOriginByPostcode($shippingAddress->getPostcode());
        $this->_shipment->setData('order_id', $order->getId());
        $this->_shipment->setData('origin_id', $origin->getId());
        $this->_shipment->setData('status', \Temando\Temando\Model\System\Config\Source\Shipment\Status::PENDING);
        $this->_shipment->setData('destination_contact_name', $shippingAddress->getName());
        $this->_shipment->setData('destination_company_name', $shippingAddress->getCompany());
        $streetAddress = $shippingAddress->getStreetLine(1);
        if ($shippingAddress->getStreetLine(2)) {
            $streetAddress .= ", " . $shippingAddress->getStreetLine(2);
        }
        $this->_shipment->setData('destination_street', $streetAddress);
        $this->_shipment->setData('destination_city', $shippingAddress->getCity());
        $this->_shipment->setData('destination_postcode', $shippingAddress->getPostcode());
        $this->_shipment->setData('destination_region', $shippingAddress->getRegion());
        $this->_shipment->setData('destination_country', $shippingAddress->getCountryId());
        $this->_shipment->setData('destination_phone', $shippingAddress->getTelephone());
        $this->_shipment->setData('destination_email', $order->getCustomerEmail());

        //set customer selected quote data
        $temandoQuoteData = explode('_', $shippingMethod);
        $temandoQuoteId = $temandoQuoteData[2];

        $temandoQuote = $this->_quote->load($temandoQuoteId);

        $carrier = $this->_helper->getCarrierByTemandoId($temandoQuote->getCarrierId());

        $quoteDescription = $carrier->getCompanyName() . ' - ' . $temandoQuote->getDeliveryMethod();

        
        $this->_shipment->setData('customer_selected_quote_id', $temandoQuoteId);
        $this->_shipment->setData('customer_selected_options', $shippingMethod);
        //$this->_shipment->setData('customer_selected_delivery_options', '');
        $this->_shipment->setData('customer_selected_quote_description', $quoteDescription);
        $this->_shipment->setData('admin_selected_quote_id', $temandoQuoteId);

        if($quoteShippingAddress->getIsBusinessAddress()){
            $this->_shipment->setData('destination_type', \Temando\Temando\Model\System\Config\Source\Origin\Type::BUSINESS);
        } else {
            $this->_shipment->setData('destination_type', \Temando\Temando\Model\System\Config\Source\Origin\Type::RESIDENTIAL);
        }

        if($quoteShippingAddress->getIsSignatureRequired()){
            $this->_shipment->setData('destination_is_signature_required', 1);
        } else {
            $this->_shipment->setData('destination_is_signature_required', 0);
        }

        try {
            $this->_shipment->save();
        } catch (\Exception $e) {
            $this->_logger->debug(__('Failed to save shipment') . ' ' . $e->getMessage());
        }
        //register the quotes with the shipment
        //$this->registerQuotes($order);
        try {
            $this->_shipment->saveAllItems();
        } catch (\Exception $e) {
            $this->_logger->debug(__('Failed to save shipment items') . ' ' . $e->getMessage());
        }


        //how many boxes required?
        $this->saveBoxes($order);

        //$this->_shipment->fetchQuotes();//or update existing quotes to use this shipment

        $this->clearSessionData();

        return $this->_shipment;
    }

    /**
     * Clear checkout session values after customer checks out
     *
     * return void
     */
    protected function clearSessionData()
    {
        $this->_checkoutSession
            ->unsetData('temando_consolidated_packaging')
            ->unsetData('selected_delivery_options')
            ->unsetData('destination_type');
    }

    /**
     * Register the quotes to the created shipment
     *
     * This is in the observer and has the order object because
     * for multishipping the order hasn't been saved yet and using
     * $shipment->getOrder() would not work in this case
     *
     * @param \Magento\Sales\Model\Order $order
     * @param \Temando\Temando\Model\Shipment $shipment
     */
    public function registerQuotes($order)
    {
        return true;
    }


    /*
     * Calulate and Save Boxes for this Order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return \Magento\Sales\Model\Order $order
     */
    public function saveBoxes($order){
        $boxesRequired = $this->_helper->calculateBoxes($order->getAllVisibleItems());
        
        foreach($boxesRequired AS $boxIndex=>$boxInfo){
            $boxLength  = $boxInfo['length'] ? $boxInfo['length'] : $this->_scopeConfig->getValue('temando/defaults/length', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $boxWidth   = $boxInfo['width'] ? $boxInfo['width'] : $this->_scopeConfig->getValue('temando/defaults/width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $boxHeight  = $boxInfo['height'] ? $boxInfo['height'] : $this->_scopeConfig->getValue('temando/defaults/height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $boxWeight  = $boxInfo['weight'] ? $boxInfo['weight'] : $this->_scopeConfig->getValue('temando/defaults/height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            $boxArticles = array();

            $this->_box
                ->setShipmentId($this->_shipment->getId())
                ->setComment($boxInfo['comment'])
                ->setQty($boxInfo['qty'])//->setQty($qty)
                ->setValue($boxInfo['value'])
                ->setLength($boxLength)
                ->setWidth($boxWidth)
                ->setHeight($boxHeight)
                ->setMeasureUnit($this->_scopeConfig->getValue('temando/units/measure', \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
                ->setWeight($boxWeight)
                ->setWeightUnit($this->_scopeConfig->getValue('temando/units/weight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE))
                ->setFragile($boxInfo['fragile'])
                ->setDangerous($boxInfo['dangerous'])
                ->setArticles(json_encode($boxInfo['comment']))
                ->save();
            //$this->_box->cleanModelCache()->clearInstance();
            $this->_box->setId(null);

        }
        return $order;
    }
}