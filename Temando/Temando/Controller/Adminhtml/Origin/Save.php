<?php
namespace Temando\Temando\Controller\Adminhtml\Origin;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Temando_Temando::temando_locations_save_origin');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        //$data = $this->_helper->transformArrayToString($data, 'store_ids');
        $data['store_ids'] = implode(',', $data['store_ids']);
        if(isSet($data['user_ids'])) {
            $data['user_ids'] = implode(',', $data['user_ids']);
        } else {
            $data['user_ids'] = '';
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Temando\Temando\Model\Origin $model */
            $model = $this->_objectManager->create('Temando\Temando\Model\Origin');

            $id = $this->getRequest()->getParam('origin_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            $this->_eventManager->dispatch(
                'temando_origin_prepare_save',
                ['origin' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved this Store.'));
                
                $response = $model->syncWarehouse();
                if (is_soap_fault($response)) {
                    $this->messageManager->addError(__('Problem synching with Temando API').': \''.$response->faultstring.'\'');
                } 
                
                
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['origin_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                //$this->messageManager->addException($e, __('Something went wrong while saving the origin.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['origin_id' => $this->getRequest()->getParam('origin_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}