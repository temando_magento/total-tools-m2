<?php
namespace Temando\Temando\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Book extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_objectManager = $objectManager;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Temando_Temando::temando_locations_book_shipment');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_logger = $this->_objectManager->create('Psr\Log\LoggerInterface');
        $this->_helper = $this->_objectManager->create('Temando\Temando\Helper\Data');

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (true) {
            /** @var \Temando\Temando\Model\Shipment $shipment */
            $shipment = $this->_objectManager->create('Temando\Temando\Model\Shipment');

            $shipment_id = $this->getRequest()->getParam('shipment');
            if ($shipment_id) {
                $shipment->load($shipment_id);
            }

            if(!$this->_helper->checkShipmentPermission($shipment)){
                $this->messageManager->addErrorMessage(__('You do not have permission to book this shipment'));
                return $resultRedirect->setPath('*/*/edit', array('shipment_id' => $shipment_id));
            }

            if (!$shipment->getOrder()->canShip()) {
                $this->messageManager->addErrorMessage(__('Order # %s cannot be shipped.', $shipment->getOrderNumber()));
                return $resultRedirect->setPath('*/*/view', array('shipment_id' => $shipment_id));
            }
            /* check if items on this shipment can be shipped */
            if (!$shipment->canShip()) {
                $this->messageManager->addErrorMessage(__('This shipment cannot be shipped.'));
                return $resultRedirect->setPath('*/*/view', array('shipment_id' => $shipment_id));
            }
//            //lock shipment resource
//            if (!$shipment->lock()) {
//                $this->_getSession()->addError($this->__('Unable to book shipment at this time. Please try again later.'));
//                $this->_redirect('*/*/edit', array('id' => $shipment_id));
//                return;
//            }
             $quote_id = $this->getRequest()->getParam('quote');

             /* @var $quote \Temando\Temando\Model\Quote */
             $quote = $this->_objectManager->create('Temando\Temando\Model\Quote');
             $quote->load($quote_id);

             $error = null;
             if (!$shipment->getId()) {
                 $this->messageManager->addErrorMessage(__('Shipment does not exist'));
             } else {
                 if (!$quote->getData('quote_id')) {
                     $error = __(
                         'Selected quote is no longer available. '
                         . 'Please refresh quotes by saving the shipment and try again'
                     );
                 } else {
                     /**
                    foreach ($shipment->getOptions() as $o) {
                        if (("insurance" == $o->getId()) && ('Y' == $o->getForcedValue())) {
                            if ('Y' != Mage::helper('temando')->getConfigData(
                                    'insurance/confirm_' . Mage::helper('temando')->getConfigData('insurance/status')
                                )
                            ) {
                                $error = Mage::helper('temando')->__(
                                    'Please agree to the insurance terms & conditions at System '
                                    . '-> Configuration -> Temando Settings -> Insurance.'
                                );
                            }
                        }
                    }*/
                    if (!$error) {
                        try {
                            $booking_result = $this->_makeBooking($shipment, $quote);

                            if($booking_result instanceof \SoapFault){
                                $errorMessage = __($booking_result->getCode()).' '.__($booking_result->getMessage());
                                $this->messageManager->addErrorMessage($errorMessage);
                                return $resultRedirect->setPath('*/*/view', array('shipment_id' => $shipment_id));

                            }
                        } catch (Exception $e) {
                            $this->_logger->debug('Book.php makeBooking exception');
                            $this->_logger->debug($e->getMessage());
                            $error = $e->getMessage();

                        }
                    }
                }
            }

            if (!$error && $booking_result) {
                try {
                    $shipment->processBookingResult($booking_result, $quote);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                }
            }
            if ($error) {
                $this->messageManager->addErrorMessage(__($error));
            } else {
                $this->messageManager->addSuccessMessage(__('Shipment booked'));
            }
            //$shipment->releaseLock();
            return $resultRedirect->setPath('*/*/view', array('shipment_id' => $shipment_id));
        }
        return $resultRedirect->setPath('*/*/');
    }


    /**
     * Make booking call
     *
     * @param \Temando\Temando\Model\Shipment $shipment
     * @param \Temando\Temando\Model\Quote $quote
     */
    protected function _makeBooking(\Temando\Temando\Model\Shipment $shipment, \Temando\Temando\Model\Quote $quote)
    {
        //$this->_logger = $this->_objectManager->create('Psr\Log\LoggerInterface');

        $scopeConfig = $this->_objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');

        /* @var $order \Magento\Sales\Model\Order */
        $order = $shipment->getOrder();

        /* @var $origin \Temando\Temando\Model\Origin */
        $origin = $this->_objectManager->create('Temando\Temando\Model\Origin');
        $origin->load($shipment->getOriginId());
        /* @var $request \Temando\Temando\Model\Api\Request */
        $request = $this->_objectManager->create('Temando\Temando\Model\Api\Request');
        $request
            ->setMagentoQuoteId($order->getQuoteId())
            ->setConnectionParams($origin->getTemandoProfile())
            ->setDestination(
                $shipment->getDestinationCountry(),
                $shipment->getDestinationPostcode(),
                $shipment->getDestinationCity(),
                $shipment->getDestinationStreet(),
                $shipment->getDestinationType()
            )
            ->setOrigin($origin->getName(), $origin->getCountry())
            ->setArticles($shipment->getArticles(true))
            ->setGoodsCurrency($order->getStore()->getCurrentCurrencyCode())
            //->setReady($this->_helper->getReadyDate())
            ->setDeliveryOptions($shipment->getDeliveryOptionsArray())
            ->setItems($shipment->getBoxes()->getItems());
//        if ($shipment->getReadyDate()) {
//            $request->setReady(strtotime($shipment->getReadyDate()), $shipment->getReadyTime());
//        } else {
//            $request->setReady($this->_helper->getReadyDate());
//        }
        $readyDate = $this->_helper->getReadyDate();
        $request->setReady($readyDate);
        $request_array = $request->toRequestArray();
        $request_array['origin'] = array(
            'description' => $origin->getName()
        );
        $request_array['destination'] = array(
            'contactName' => $shipment->getDestinationContactName(),
            'companyName' => $shipment->getDestinationCompanyName(),
            'street' => $shipment->getDestinationStreet(),
            'suburb' => $shipment->getDestinationCity(),
            'city' => $shipment->getDestinationRegion(),
            'code' => $shipment->getDestinationPostcode(),
            'country' => $shipment->getDestinationCountry(),
            'phone1' => preg_replace('/\D/', '', $shipment->getDestinationPhone()),
            'phone2' => '',
            'fax' => '',
            'email' => $shipment->getDestinationEmail(),
        );

//        $instructions = $shipment->getShippingInstructions();
//        if ($instructions) {
//            $request_array['instructions'] = $instructions;
//        }

        //$option_array = $shipment->getOptionsArray();
//        if (!is_null(Mage::getSingleton('adminhtml/session')->getData('insurance_' . $shipment->getId()))) {
//            $option_array['insurance'] =
//                Mage::getSingleton('adminhtml/session')->getData('insurance_' . $shipment->getId());
//        }
        $request_array['quote'] = $quote->toBookingRequestArray();
        $request_array['payment'] = array(
            'paymentType' => 'Account',//@todo Mage::helper('temando')->getConfigData('general/payment_type'),
        );

        $labelType = $scopeConfig->getValue('temando/options/label_type',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
//        if ($origin->getLabelType()) {
//            $labelType = $origin->getLabelType();
//        }
        if ($labelType) {
            $request_array['labelPrinterType'] = $labelType;
        }
        $request_array['reference'] = $order->getIncrementId();


        /* @var $api /Temando/Temando/Model/Api/Client */
        $api = $this->_objectManager->create('\Temando\Temando\Model\Api\Client');

        $api->connect($origin->getTemandoProfile());
//        $this->_logger->debug('Book.php makeBooking Request Array');
//        $this->_logger->debug(print_r($request_array, 1));
        return $api->makeBooking($request_array);
    }
    
}