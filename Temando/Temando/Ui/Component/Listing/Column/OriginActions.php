<?php
namespace Temando\Temando\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

class OriginActions extends Column
{
    /** Url path */
    const TEMANDO_ORIGIN_URL_PATH_EDIT = 'temando/origin/edit';
    const TEMANDO_ORIGIN_URL_PATH_DELETE = 'temando/origin/delete';

    /** @var UrlInterface */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * Temando Helper
     *
     * @var \Temando\Temando\Helper\Data
     */
    protected $_helper;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param \Temando\Temando\Helper\Data $helper
     * @param array $components
     * @param array $data
     * @param string $editUrl
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Magento\Backend\Block\Template\Context $templateContext,
        \Temando\Temando\Helper\Data $helper,
        array $components = [],
        array $data = [],
        $editUrl = self::TEMANDO_ORIGIN_URL_PATH_EDIT
    ) {
//        $this->_authorization = $templateContext->getAuthorization();
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        $this->_helper = $helper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['origin_id'])) {
                    if ($this->_helper->_isAllowedAction('Temando_Temando::temando_locations_edit_origin')) {
                        $item[$name]['edit'] = [
                            'href' => $this->urlBuilder->getUrl($this->editUrl, ['origin_id' => $item['origin_id']]),
                            'label' => __('Edit')
                        ];
                    }
                    if ($this->_helper->_isAllowedAction('Temando_Temando::temando_locations_delete_origin')) {
                        $item[$name]['delete'] = [
                            'href' => $this->urlBuilder->getUrl(self::TEMANDO_ORIGIN_URL_PATH_DELETE, ['origin_id' => $item['origin_id']]),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete "${ $.$data.name }"'),
                                'message' => __('Are you sure you wan\'t to delete a "${ $.$data.name }" record?')
                            ]
                        ];
                    }
                }
            }
        }

        return $dataSource;
    }

}